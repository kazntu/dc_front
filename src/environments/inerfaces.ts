export interface Environment {
  production: boolean;
  siteUrl: string;
}
