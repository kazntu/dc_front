import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Country} from '../../shared/interfaces';

import {AlertService} from '../../shared/services/alert.service';
import {CountriesService} from '../../shared/services/countries.service';

@Component({
  selector: 'app-create-country',
  templateUrl: './create-country.component.html',
  styleUrls: ['./create-country.component.scss']
})
export class CreateCountryComponent implements OnInit {
  form: FormGroup;

  constructor(private countriesService: CountriesService, private alert: AlertService) {}

  ngOnInit() {
    this.form = new FormGroup({
      code: new FormControl(null, Validators.required),
      title: new FormControl(null, Validators.required),
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    const country: Country = {
      code: this.form.value.code,
      title: this.form.value.title,
    };

    this.countriesService.create(country).subscribe(() => {
      this.form.reset();
      this.alert.success('Запись была создана');
    });
  }
}
