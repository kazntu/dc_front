import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {Country} from '../../shared/interfaces';
import {CountriesService} from '../../shared/services/countries.service';
import {AlertService} from '../../shared/services/alert.service';

@Component({
  selector: 'app-edit-country',
  templateUrl: './edit-country.component.html',
  styleUrls: ['./edit-country.component.scss']
})
export class EditCountryComponent implements OnInit, OnDestroy {
  form: FormGroup;
  country: Country;
  submitted = false;

  uSub: Subscription;

  constructor(private route: ActivatedRoute, private countriesService: CountriesService, private alert: AlertService) {}

  ngOnInit() {
    this.route.params.pipe(
      switchMap((params: Params) => {
        return this.countriesService.getById(params.id);
      })
    ).subscribe((country: Country) => {
      this.country = country;
      this.form = new FormGroup({
        title: new FormControl(country.title, Validators.required),
        code: new FormControl(country.code, Validators.required),
      });
    });
  }

  ngOnDestroy() {
    if (this.uSub) {
      this.uSub.unsubscribe();
    }
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    this.submitted = true;

    this.uSub = this.countriesService.update({
      ...this.country,
      title: this.form.value.title,
      code: this.form.value.code,
    }).subscribe(() => {
      this.submitted = false;
      this.alert.success('Запись была обновлена');
    });
  }
}
