import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {fromEvent, Subscription} from 'rxjs';
import {AlertService} from '../../shared/services/alert.service';
import {CountriesService} from '../../shared/services/countries.service';
import {Country, Pagination} from '../../shared/interfaces';
import {debounceTime, map} from 'rxjs/operators';

@Component({
  selector: 'app-list-country',
  templateUrl: './list-country.component.html',
  styleUrls: ['./list-country.component.scss']
})
export class ListCountryComponent implements OnInit, OnDestroy {
  countries: Country[] = [];
  pagination: Pagination;

  rSub: Subscription;
  pSub: Subscription;
  dSub: Subscription;
  sSub: Subscription;

  title = '';

  pageNumber = 1;

  constructor(
    private countriesService: CountriesService,
    private alert: AlertService
  ) {}

  ngOnInit() {
    this.sSub = fromEvent(document.getElementsByClassName('filter'), 'keyup').pipe(
      debounceTime(600),
      map( () => this.showList())
    ).subscribe();

    this.showList();
  }

  showList(pageNumber?) {
    if (pageNumber) {
      this.pageNumber = pageNumber;
    }

    this.pSub = this.countriesService.getAll({pageNumber: this.pageNumber, title: this.title})
      .subscribe(countries => {
        this.countries = countries.list;
        this.pagination = countries.pagination;
      });
  }

  remove(id: string) {
    if (!confirm('Подтвердите удаление записи')) {
      return;
    }

    this.dSub = this.countriesService.remove(id).subscribe(() => {
      this.countries = this.countries.filter(country => country.id !== id);
      this.alert.warning('Запись была удалена');
    });
  }

  ngOnDestroy() {
    if (this.pSub) { this.pSub.unsubscribe(); }
    if (this.sSub) { this.sSub.unsubscribe(); }
    if (this.rSub) { this.rSub.unsubscribe(); }
    if (this.dSub) { this.dSub.unsubscribe(); }
  }
}
