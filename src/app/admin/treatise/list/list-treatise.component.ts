import {Component, OnDestroy, OnInit} from '@angular/core';
import {fromEvent, Subscription} from 'rxjs';
import {AlertService} from '../../shared/services/alert.service';
import {TreatisesService} from '../../shared/services/treatises.service';
import {Treatise, Pagination} from '../../shared/interfaces';
import {debounceTime, map} from 'rxjs/operators';

@Component({
  selector: 'app-list-treatise',
  templateUrl: './list-treatise.component.html',
  styleUrls: ['./list-treatise.component.scss']
})
export class ListTreatiseComponent implements OnInit, OnDestroy {
  treatises: Treatise[] = [];
  pagination: Pagination;

  rSub: Subscription;
  pSub: Subscription;
  dSub: Subscription;
  sSub: Subscription;

  treatiseName = '';
  subjectLastName = '';
  subjectFirstName = '';
  subjectMiddleName = '';
  subjectIin = '';
  issn = '';
  isbn = '';

  pageNumber = 1;

  constructor(private treatisesService: TreatisesService, private alert: AlertService) {}

  ngOnInit() {
    this.sSub = fromEvent(document.getElementsByClassName('filter'), 'keyup').pipe(
      debounceTime(600),
      map( () => this.showList())
    ).subscribe();

    this.showList();
  }

  showList(pageNumber?) {
    if (pageNumber) {
      this.pageNumber = pageNumber;
    }

    this.pSub = this.treatisesService.getAll({
      pageNumber: this.pageNumber,
      treatiseName: this.treatiseName,
      issn: this.issn,
      isbn: this.isbn,
      subjectLastName: this.subjectLastName,
      subjectFirstName: this.subjectFirstName,
      subjectMiddleName: this.subjectMiddleName,
      subjectIin: this.subjectIin,
    })
      .subscribe(treatises => {
        this.treatises = treatises.list;
        this.pagination = treatises.pagination;
      });
  }

  remove(id: string) {
    if (!confirm('Подтвердите удаление записи')) {
      return;
    }

    this.dSub = this.treatisesService.remove(id).subscribe(() => {
      this.treatises = this.treatises.filter(treatise => treatise.id !== id);
      this.alert.warning('Запись была удалена');
    });
  }

  ngOnDestroy() {
    if (this.pSub) { this.pSub.unsubscribe(); }
    if (this.sSub) { this.sSub.unsubscribe(); }
    if (this.rSub) { this.rSub.unsubscribe(); }
    if (this.dSub) { this.dSub.unsubscribe(); }
  }
}
