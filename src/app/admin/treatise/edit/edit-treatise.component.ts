import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {Treatise} from '../../shared/interfaces';
import {TreatisesService} from '../../shared/services/treatises.service';
import {AlertService} from '../../shared/services/alert.service';

@Component({
  selector: 'app-edit-treatise',
  templateUrl: './edit-treatise.component.html',
  styleUrls: ['./edit-treatise.component.scss']
})
export class EditTreatiseComponent implements OnInit, OnDestroy {
  form: FormGroup;
  treatise: Treatise;
  submitted = false;

  uSub: Subscription;

  constructor(private route: ActivatedRoute, private treatisesService: TreatisesService, private alert: AlertService) {}

  ngOnInit() {
    this.route.params.pipe(
      switchMap((params: Params) => {
        return this.treatisesService.getById(params.id);
      })
    ).subscribe((treatise: Treatise) => {
      this.treatise = treatise;
      this.form = new FormGroup({
        authorId: new FormControl(treatise.authorId, Validators.required),
        treatiseName: new FormControl(treatise.treatiseName, Validators.required),
        issn: new FormControl(treatise.issn, Validators.required),
        isbn: new FormControl(treatise.isbn, Validators.required),
        issueYear: new FormControl(treatise.issueYear, Validators.required),
        numPages: new FormControl(treatise.numPages, Validators.required),
        treatiseLangEn: new FormControl(treatise.treatiseLangEn, Validators.required),
        treatiseLangRu: new FormControl(treatise.treatiseLangRu, Validators.required),
        treatiseLangKz: new FormControl(treatise.treatiseLangKz, Validators.required),
        publishedScopus: new FormControl(treatise.publishedScopus, Validators.required),
        publishedWebOfScience: new FormControl(treatise.publishedWebOfScience, Validators.required),
        publishingHouse: new FormControl(treatise.publishingHouse, Validators.required),
      });
    });
  }

  ngOnDestroy() {
    if (this.uSub) {
      this.uSub.unsubscribe();
    }
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    this.submitted = true;

    this.uSub = this.treatisesService.update({
      ...this.treatise,
      authorId: this.form.value.authorId,
      treatiseName: this.form.value.treatiseName,
      issn: this.form.value.issn,
      isbn: this.form.value.isbn,
      issueYear: this.form.value.issueYear,
      numPages: this.form.value.numPages,
      treatiseLangEn: this.form.value.treatiseLangEn,
      treatiseLangRu: this.form.value.treatiseLangRu,
      treatiseLangKz: this.form.value.treatiseLangKz,
      publishedScopus: this.form.value.publishedScopus,
      publishedWebOfScience: this.form.value.publishedWebOfScience,
      publishingHouse: this.form.value.publishingHouse
    }).subscribe(() => {
      this.submitted = false;
      this.alert.success('Запись была обновлена');
    });
  }
}
