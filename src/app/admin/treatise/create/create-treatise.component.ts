import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Treatise} from '../../shared/interfaces';

import {AlertService} from '../../shared/services/alert.service';
import {TreatisesService} from '../../shared/services/treatises.service';

@Component({
  selector: 'app-create-treatise',
  templateUrl: './create-treatise.component.html',
  styleUrls: ['./create-treatise.component.scss']
})
export class CreateTreatiseComponent implements OnInit {
  form: FormGroup;

  constructor(private treatisesService: TreatisesService, private alert: AlertService) {}

  ngOnInit() {
    this.form = new FormGroup({
      authorId: new FormControl(null, Validators.required),
      treatiseName: new FormControl(null, Validators.required),
      issn: new FormControl(null, Validators.required),
      isbn: new FormControl(null, Validators.required),
      issueYear: new FormControl(null, Validators.required),
      numPages: new FormControl(null, Validators.required),
      treatiseLangEn: new FormControl(null, Validators.required),
      treatiseLangRu: new FormControl(null, Validators.required),
      treatiseLangKz: new FormControl(null, Validators.required),
      publishedScopus: new FormControl(null, Validators.required),
      publishedWebOfScience: new FormControl(null, Validators.required),
      publishingHouse: new FormControl(null, Validators.required),
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    const treatise: Treatise = {
      authorId: this.form.value.authorId,
      treatiseName: this.form.value.treatiseName,
      issn: this.form.value.issn,
      isbn: this.form.value.isbn,
      issueYear: this.form.value.issueYear,
      numPages: this.form.value.numPages,
      treatiseLangEn: this.form.value.treatiseLangEn,
      treatiseLangRu: this.form.value.treatiseLangRu,
      treatiseLangKz: this.form.value.treatiseLangKz,
      publishedScopus: this.form.value.publishedScopus,
      publishedWebOfScience: this.form.value.publishedWebOfScience,
      publishingHouse: this.form.value.publishingHouse
    };

    this.treatisesService.create(treatise).subscribe(() => {
      this.form.reset();
      this.alert.success('Запись была создана');
    });
  }
}
