export interface Pagination {
  currentPage: number;
  lastPage: number;
  perPage: number;
  total: number;
}

export interface PaginationMetaResponse {
  current_page: number;
  from: string;
  last_page: number;
  path: string;
  per_page: number;
  to: string;
  total: number;
}

export interface PaginationLinksResponse {
  first: string;
  last: string;
  next: string;
  prev: string;
}

export interface User {
  email: string;
  password: string;
  returnSecureToken?: boolean;
}

export interface FbAuthResponse {
  idToken: string;
  expiresIn: string;
}

export interface Country {
  id?: string;
  code: string;
  title: string;
}

export interface CountryList {
  list: Country[];
  pagination: Pagination;
}

export interface CountryResponse {
  id?: string;
  code: string;
  title: string;
}

export interface Treatise {
  id?: string;
  authorId: string;
  treatiseName: string;
  issn: string;
  isbn: string;
  issueYear: string;
  numPages: string;
  treatiseLangEn: string;
  treatiseLangRu: string;
  treatiseLangKz: string;
  publishedScopus: string;
  publishedWebOfScience: string;
  publishingHouse: string;
  lastName?: string;
  firstName?: string;
  middleName?: string;
}

export interface TreatiseList {
  list: Treatise[];
  pagination: Pagination;
}

export interface TreatiseResponse {
  id?: string;
  f_AUTHOR_ID: string;
  f_TREATISE_NAME: string;
  f_ISSN: string;
  f_ISBN: string;
  f_ISSUE_YEAR: string;
  f_NUM_PAGES: string;
  f_TREATISE_LANG_EN: string;
  f_TREATISE_LANG_RU: string;
  f_TREATISE_LANG_KZ: string;
  f_PUBLISHED_SCOPUS: string;
  f_PUBLISHED_WEB_OF_SCIENCE: string;
  PUBLISHING_HOUSE: string;
}
