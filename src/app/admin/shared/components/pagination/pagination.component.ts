import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Pagination} from '../../interfaces';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input() items: Pagination;
  @Output() changePagination = new EventEmitter();

  constructor() { }

  result = [];
  pageNumb : number;


  ngOnInit() {
    this.pageNumb = Math.ceil(this.items.total / this.items.perPage);
    for (let i=1; i <= this.pageNumb; i++){
      this.result[i-1] = i;
    }
  }
}
