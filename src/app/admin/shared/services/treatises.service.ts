import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {
  Treatise,
  TreatiseList,
  TreatiseResponse
} from '../interfaces';
import {Func} from './func.service';

@Injectable({
  providedIn: 'root'
})
export class TreatisesService {
  fields = {
    0: {f: 'id', b: 'id'},
    1: {f: 'authorId', b: 'f_AUTHOR_ID'},
    2: {f: 'treatiseName', b: 'f_TREATISE_NAME'},
    3: {f: 'issn', b: 'f_ISSN'},
    4: {f: 'isbn', b: 'f_ISBN'},
    5: {f: 'issueYear', b: 'f_ISSUE_YEAR'},
    6: {f: 'numPages', b: 'f_NUM_PAGES'},
    7: {f: 'treatiseLangEn', b: 'f_TREATISE_LANG_EN'},
    8: {f: 'treatiseLangRu', b: 'f_TREATISE_LANG_RU'},
    9: {f: 'treatiseLangKz', b: 'f_TREATISE_LANG_KZ'},
    10: {f: 'publishedScopus', b: 'f_PUBLISHED_SCOPUS'},
    11: {f: 'publishedWebOfScience', b: 'f_PUBLISHED_WEB_OF_SCIENCE'},
    12: {f: 'publishingHouse', b: 'PUBLISHING_HOUSE'},
    13: {f: 'lastName', b: 'f_LAST_NAME'},
    14: {f: 'firstName', b: 'f_FIRST_NAME'},
    15: {f: 'middleName', b: 'f_MIDDLE_NAME'},
  };

  constructor(private http: HttpClient) {}

  create(treatise: Treatise): Observable<Treatise> {
    return this.http.post<Treatise>(environment.siteUrl + '/v1/treatise', Func.getBackFields(this.fields, treatise));
  }

  update(treatise: Treatise): Observable<Treatise> {
    return this.http.patch<Treatise>(environment.siteUrl + '/v1/treatise/' + treatise.id, Func.getBackFields(this.fields, treatise));
  }

  remove(id: string): Observable<void> {
    return this.http.delete<void>(environment.siteUrl + '/v1/treatise/' + id);
  }

  getById(id: string): Observable<any> {
    return this.http.get<any>(environment.siteUrl + '/v1/treatise/' + id)
      .pipe(map((response: {data: TreatiseResponse}) => {
        return Func.getFrontFields(this.fields, response.data);
      }));
  }

  getAll(data: {
    pageNumber?: number,
    treatiseName?: string,
    issn?: string,
    isbn?: string,
    subjectLastName?: string,
    subjectFirstName?: string,
    subjectMiddleName?: string,
    subjectIin?: string
  }): Observable<TreatiseList> {
    const query = this.http.get(environment.siteUrl + '/v1/treatise?'
      + 'page=' + ((!data.pageNumber) ? 1 : data.pageNumber)
      + (data.treatiseName ? '&treatiseName=' + data.treatiseName : '')
      + (data.issn ? '&issn=' + data.issn : '')
      + (data.isbn ? '&isbn=' + data.isbn : '')
      + (data.subjectLastName ? '&subjectLastName=' + data.subjectLastName : '')
      + (data.subjectFirstName ? '&subjectFirstName=' + data.subjectFirstName : '')
      + (data.subjectMiddleName ? '&subjectMiddleName=' + data.subjectMiddleName : '')
      + (data.subjectIin ? '&subjectIin=' + data.subjectIin : '')
    );

    return Func.getListAndPagination(query, this.fields);
  }
}
