import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {
  Country,
  CountryResponse,
  CountryList
} from '../interfaces';
import {Func} from './func.service';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {
  fields = {
    0: {f: 'id', b: 'id'},
    1: {f: 'title', b: 'title'},
    2: {f: 'code', b: 'code'}
  };

  constructor(private http: HttpClient) {}

  create(country: Country): Observable<Country> {
    return this.http.post<Country>(environment.siteUrl + '/v1/country', Func.getBackFields(this.fields, country));
  }

  update(country: Country): Observable<Country> {
    return this.http.patch<Country>(environment.siteUrl + '/v1/country/' + country.id, Func.getBackFields(this.fields, country));
  }

  remove(id: string): Observable<void> {
    return this.http.delete<void>(environment.siteUrl + '/v1/country/' + id);
  }

  getById(id: string): Observable<any> {
    return this.http.get<any>(environment.siteUrl + '/v1/country/' + id)
      .pipe(map((response: {data: CountryResponse}) => {
        return Func.getFrontFields(this.fields, response.data);
      }));
  }

  getAll(data: {
    pageNumber?: number,
    title?: string
  }): Observable<CountryList> {
    const query = this.http.get(environment.siteUrl + '/v1/country?'
      + 'page=' + ((!data.pageNumber) ? 1 : data.pageNumber)
      + (data.title ? '&title=' + data.title : '')
    );

    return Func.getListAndPagination(query, this.fields);
  }
}
