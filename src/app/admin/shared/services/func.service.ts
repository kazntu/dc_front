import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {Pagination, PaginationLinksResponse, PaginationMetaResponse} from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class Func {
  /**
   * @param fields названия полей
   * @param data поля из бекенда заполненные данными
   */
  static getFrontFields(fields, data) {
    const result = {};

    Object.keys(fields).forEach((item) => {
      result[fields[item].f] = data[fields[item].b];
    });

    return result;
  }

  /**
   * @param fields названия полей
   * @param data поля из фронта заполненные данными
   */
  static getBackFields(fields, data) {
    const result = {};

    Object.keys(fields).forEach((item) => {
        result[fields[item].b] = data[fields[item].f];
    });

    return result;
  }

  /**
   * Получаем поля и пагинацию
   * @param query запрос на получение списка
   * @param fields поля из бекенда заполненные данными
   */
  static getListAndPagination(query, fields) {
    return query.pipe(
      map((response: {data: {}, meta: PaginationMetaResponse, links: PaginationLinksResponse}): any => {
        const list = Object
          .keys(response.data)
          .map(i => Func.getFrontFields(fields, response.data[i]));

        const pagination: Pagination = {
          currentPage: response.meta.current_page,
          lastPage: response.meta.last_page,
          perPage: response.meta.per_page,
          total: response.meta.total,
        };

        return {
          list,
          pagination
        };
      })
    );
  }
}
