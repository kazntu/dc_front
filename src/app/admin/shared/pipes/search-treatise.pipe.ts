import {Pipe, PipeTransform} from '@angular/core';
import {Treatise} from '../interfaces';

@Pipe({
  name: 'searchTreatise'
})
export class SearchTreatisePipe implements PipeTransform {
  transform(treatises: Treatise[], search = ''): Treatise[] {
    if (!search.trim()) {
      return treatises;
    }

    return treatises.filter(treatise => {
      return treatise.treatiseName.toLowerCase().includes(search.toLowerCase());
    });
  }
}
