import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {registerLocaleData} from '@angular/common';
import ruLocale from '@angular/common/locales/ru';
import {RouterModule} from '@angular/router';
import { AdminLayoutComponent } from './shared/components/admin-layout/admin-layout.component';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {AuthGuard} from './shared/services/auth.guard';
import { AlertComponent } from './shared/components/alert/alert.component';
import {AlertService} from './shared/services/alert.service';
import {CreateCountryComponent} from './country/create/create-country.component';
import {ListCountryComponent} from './country/list/list-country.component';
import {SearchCountryPipe} from './shared/pipes/search-country.pipe';
import {EditCountryComponent} from './country/edit/edit-country.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import { PaginationComponent } from './shared/components/pagination/pagination.component';
import {CreateTreatiseComponent} from './treatise/create/create-treatise.component';
import {ListTreatiseComponent} from './treatise/list/list-treatise.component';
import {EditTreatiseComponent} from './treatise/edit/edit-treatise.component';
import {SearchTreatisePipe} from './shared/pipes/search-treatise.pipe';
import {NgSelectModule} from '@ng-select/ng-select';

registerLocaleData(ruLocale, 'ru');

@NgModule({
  declarations: [
    AdminLayoutComponent,
    LoginComponent,
    AlertComponent,

    DashboardComponent,

    CreateCountryComponent,
    ListCountryComponent,
    SearchCountryPipe,
    EditCountryComponent,

    CreateTreatiseComponent,
    ListTreatiseComponent,
    SearchTreatisePipe,
    EditTreatiseComponent,

    PaginationComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '', component: AdminLayoutComponent, children: [
          {path: '', redirectTo: '/admin/dashboard', pathMatch: 'full'},
          {path: 'login', component: LoginComponent},

          {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},

          {path: 'country/:id/edit', component: EditCountryComponent, canActivate: [AuthGuard]},
          {path: 'country/create', component: CreateCountryComponent, canActivate: [AuthGuard]},
          {path: 'country', component: ListCountryComponent, canActivate: [AuthGuard]},

          /*научный труд*/
          {path: 'treatise/:id/edit', component: EditTreatiseComponent, canActivate: [AuthGuard]},
          {path: 'treatise/create', component: CreateTreatiseComponent, canActivate: [AuthGuard]},
          {path: 'treatise', component: ListTreatiseComponent, canActivate: [AuthGuard]},
        ]
      }
    ]),
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    NgSelectModule,
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthGuard,
    AlertService,
  ]
})
export class AdminModule {

}
